<?php

// 经过判断或函数运算才能进行定义的常量
define('BAIDUPCS_APP_TOKEN', get_option('baidupcs_app_token'));
define('BAIDUPCS_SITE_DOMAIN', $_SERVER['HTTP_HOST']);
define('BAIDUPCS_REMOTE_ROOT_PATH', '/apps/drupalpcs/' . BAIDUPCS_SITE_DOMAIN);
define('BAIDUPCS_REMOTE_BACKUP_PATH', BAIDUPCS_REMOTE_ROOT_PATH.'/backup');
define('WP2PCS_TMP_PATH', get_real_path(ABSPATH.'/wp2pcs_tmp'));


