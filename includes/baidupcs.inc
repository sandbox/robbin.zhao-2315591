<?php

// 创建一个函数直接将单个文件送到百度盘
function wp_backup_to_pcs_send_single_file($local_path,$remote_dir){
  global $BaiduPCS;
  $file_name = basename($local_path);
  $file_size = filesize($local_path);
  $handle = @fopen($local_path,'rb');
  $file_content = fread($handle,$file_size);
  $BaiduPCS->upload($file_content,trailing_slash_path($remote_dir),$file_name);
  fclose($handle);
  @unlink($local_path);
}

// 超大文件分片上传函数
function wp_backup_to_pcs_send_super_file($local_path,$remote_dir){
  global $BaiduPCS;
  $file_name = basename($local_path);
  
  $file_blocks = array();//分片上传文件成功后返回的md5值数组集合
  $handle = @fopen($local_path,'rb');
  while(!@feof($handle)){
    $file_block_content = fread($handle,2*1024*1024);
    $temp = $BaiduPCS->upload($file_block_content,trailing_slash_path($remote_dir),$file_name,false,true);
    if(!is_array($temp)){
      $temp = json_decode($temp,true);
    }
    if(isset($temp['md5'])){
      array_push($file_blocks,$temp['md5']);
    }
  }
  fclose($handle);
  @unlink($local_path);
  if(count($file_blocks) > 1){
    $BaiduPCS->createSuperFile(trailing_slash_path($remote_dir),$file_name,$file_blocks,'');
  }
}

// 创建一个函数来确定采取什么上传方式，并执行这种方式的上传
function wp_backup_to_pcs_send_file($local_path,$remote_dir){
  $file_name = basename($local_path);
  $file_size = get_real_filesize($local_path);
  $file_max_size = 20*1024*1024;
  if($file_size > $file_max_size){
    wp_backup_to_pcs_send_super_file($local_path,$remote_dir);
  }else{
    wp_backup_to_pcs_send_single_file($local_path,$remote_dir);
  }
}