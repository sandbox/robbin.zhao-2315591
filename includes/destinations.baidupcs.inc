<?php


/**
 * @file
 * Functions to handle the Baidu cloud backup destination.
 */

/**
 * A destination for Baidu cloud database backups.
 *
 * @ingroup backup_migrate_destinations
 */
class backup_migrate_destination_baidupcs extends backup_migrate_destination_remote {
  var $supported_ops = array('scheduled backup', 'manual backup', 'configure');
  /**
   * Get the form for the settings for this filter.
   */
  function edit_form() {
    $form = parent::edit_form();
    $form['location'] = array(
      "#type"    => "textfield",
      "#title"   => t("Directory path"),
      "#default_value" => 'Baidu Cloud',
      "#required" => TRUE,
      "#description" => t('Enter the path to the directory to save the backups to. Use a relative path to pick a path relative to your Drupal root directory. The web server must be able to write to this path.'),
    );

    $form['user']['#title'] = t('Access Key ID');
    $form['pass']['#title'] = t('Secret Access Key');
    return $form;
  }
  
  function type_name() {
    return t("Baidu PCS");
  }
  /**
   * Backup
   */
  function save_file($file, $settings) {
    $size = filesize($file->filepath());
    $attachment = new stdClass();
    $attachment->filename = $file->filename();
    $attachment->path = $file->filepath();
    
    $account = new stdClass();
    $success = _backup_migrate_destination_baidu_backup($attachment, $settings);
    if($success){
      drupal_set_message('Upload successfully');
      return $file;
    }
    
    return null;
  }
  
  /**
   * Validate the configuration form. Make sure the baidu account is valid.
   */
  function edit_form_validate($form, &$form_state) {
    _baidu_library_load();
    $bucket = $form_state['values']['settings']['bucket'];
    $object = $form_state['values']['settings']['object'];
    if(!BaiduBCS::validate_bucket($bucket)) {
      form_set_error('[settings][bucket]', t('The Bucket %bucket is not valid.', array('%bucket' => $bucket)));
    }
    if(!BaiduBCS::validate_object($object)) {
      form_set_error('[settings][object]', t('The Object %$object is not valid.', array('%$object' => $object)));
    }
  }
}

function _backup_migrate_destination_baidu_backup($attachment, $settings) {
  $bucket_name = $settings->destination->settings['bucket'];
  $file = $attachment->path;
  $object = $settings->destination->settings['object'].date("YmdHis");
  $ak = $settings->destination->settings['ak'];
  $sk = $settings->destination->settings['sk'];
  //Send to baidu pan
  $baidu_cloud = new baiduCloud($ak, $sk, $bucket_name, $object, $file);
  
  return $baidu_cloud->uploadFileToBaiduCloud();
}